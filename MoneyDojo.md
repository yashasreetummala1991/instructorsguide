- **The purpose of this session is to have a basic understanding of the core concepts of Value Objects including the concept of equality.**

# Learning Objectives
By the end of this session, the learner should be able to:
-   Understand the concept of value objects
-   Know how to implement proper equals methods
-   Describe the clauses of generated equals methods
-   Relate the purpose of the hashCode method, and equals/hashCode connection
-   List concepts that may be manifested as value objects in a system
-   Use immutable value objects

# Facilitation style
- This session typically runs for a day.
- Make sure the Pairing is done prior to the session.
- Ask people to put more attention towards the requirement given as requirements might be tricky for this dojo. 
- Try to use polling option in zoom atleast for first two requirements.

# Competencies in focus
- [[Clean code competency]]
- [[Emergent design competency]]
- [[TDD Competency]]
- [[Object Oriented Design Compentency]]
- [[Using your tools well competency]]

# Learning objectives
## IntelliJ 
- This is a good opportunity to re-emphasise on being able to switch between test & source files `cmd + shift + t`
- Also `cmd + B` to go inside any method


## Testable solutions
- reinforce how test acts as living documentation. They understood the existing behavior of Mars rover by simply looking at its test cases.

## OOP
- Identify the need of domain objects like
	- `Position`
	- `Coordinate`
	- `Direction`
	- `Rover`
- Identify objects by using separation of concerns
	- Separating `Parser`  From the `Rover`
- Identify the case for Polymorphism with directions and commands

## Clean code
- There are many existing code smells in the code
	- primitive obsession
		- Representing position as an array
		- Representing direction as strings 
		- Representing commands as strings
	- catching a more general exception instead of specific exception
		- Currently we catch `RuntimeException` Instead of `NumberFormatException` When trying to use `Integer.parseInt`
	- God class
		- Given the coupling of parsing, translation to domain model and using the domain model to achieve the business case is all within one class – there is no doubt that this is a God class.
	- long method
		- All of the code (input parsing moving the rover,...) currently exists in a single `run` method in the God class.
	- Magic numbers
		- Given all the parsing happens in the Mars rover class there is no doubt that the code is littered with numbers like zero, one or two which have very specific meaning. This meaning is not easily conveyed.
	- multi-level nesting
		- Multiple if...else blocks for commands and direction inside a for loop
- Coupling and breaking of single responsibility principle
	- The Mars rover class is currently responsible both for parsing the user input, translating it into a domain model and then doing calculations for the final position
- Identify the code smell of a long conditional

## Re-factoring 
- 
- Emphasize that refactoring should not break any existing functionality
- Because of the many code smells there are many opportunities to use different refactoring.
- small commits

## Java
- Talk about how [[enums in java]] are essentially object created at compile time and can be bundled with arbitrary functionality (most of the time trainees think that [[enum with just values|enums are just used for naming values]], but don’t realise that [[enum with a method now|enum can have arbitrary methods]])
	- Also this is good time to reinforce that [[enums can hold arbitrary data through constructor]] and are [[different enum values relate to each other with indexes]]
	- The trainees might have already learned this as part of [[Money dojo]], Hence Mars rover in this sense acts like a reinforcement exercise.

## Domain driven design
- Identify domain objects and see if it’s an [[entity (in OOP)]] or [[value object]]
	- Identifying `Currency` as [[value object]] and Rover as [[entity (in OOP)]]

## Design pattern
- This exercise provides an opportunity to talk about the [[command design pattern]] 


# After session
- It might not be possible to work on all requirements. Leave other requirements as an assignment to the trainees.
- Given this session exposes people to a lot of `assert statements`, this is an opportunity to explore more in JUnit.
- Trainees can also be encouraged to give a talk on `Fluent API's`

# Resources 
- [Value object - Wikipedia, the free encyclopaedia](http://en.wikipedia.org/wiki/Value_object)
- [introduceParameterObject](http://www.refactoring.com/catalog/introduceParameterObject.html)
- Value objects - [https://martinfowler.com/bliki/ValueObject.html](https://martinfowler.com/bliki/ValueObject.html)
- Equals and HashCode - [https://dzone.com/articles/working-with-hashcode-and-equals-in-java](https://dzone.com/articles/working-with-hashcode-and-equals-in-java)

# contributors


